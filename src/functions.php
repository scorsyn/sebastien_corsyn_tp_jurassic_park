<?php

use Michelf\Markdown;

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return Markdown::defaultTransform($string_markdown_formatted);
}

function readFileContent($filepath)
{
    return file_get_contents($filepath);
}

function getDinos()
{
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getDinoInfos($slug)
{
    $path = sprintf("https://medusa.delahayeyourself.info/api/dinosaurs/%s", $slug);
    $response = Requests::get($path);
    return json_decode($response->body);
}

function randomDinos()
{
    $dinos = getDinos();
    $keys = array_rand($dinos, 3);
    $top_rated_dinos = array($dinos[$keys[0]], $dinos[$keys[1]], $dinos[$keys[2]]);
    return $top_rated_dinos;
}
