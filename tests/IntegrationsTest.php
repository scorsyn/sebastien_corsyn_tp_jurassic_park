<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest
{

    public function test_dinos()
    {
        $response = $this->make_request("GET", "/");
        $dinos = getDinos();
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        foreach ($dinos as $dino) {
            $this->assertStringContainsString($dino->name, $body);
        }
    }

    public function test_dinoinfos()
    {
        $response = $this->make_request("GET", "/dinosaur/velociraptor");
        $dino = getDinoInfos("velociraptor");
        $this->assertEquals(200, $response->getStatusCode());
        $body = $response->getBody()->getContents();
        $this->assertStringContainsString($dino->name, $body);
    }

}
