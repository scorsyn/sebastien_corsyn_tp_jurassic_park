<?php

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase
{

    public function test_GetDinos()
    {
        $dinos = getDinos();
        $this->assertIsArray($dinos);
        $this->assertEquals(7, count($dinos));
        $this->assertIsString($dinos[0]->name);
    }

    public function test_GetDinoInfos()
    {
        $dino = getDinoInfos("brachiosaurus");
        $this->assertInstanceOf(stdClass::class, $dino);
        $this->assertIsString($dino->avatar);
    }

}
