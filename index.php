<?php
require "vendor/autoload.php"; //inclusion de l'autoloader

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension


$twig->addFilter(new Twig_Filter('markdown', function($string){
    return \Michelf\MarkdownExtra::defaultTransform($string);
}, ['is_safe' => ['html']]));

});

Flight::route('/', function () {
    $data = [
        'dinos' => getDinos(),
    ];
    Flight::view()->display('list.twig', $data);
});

Flight::route('/dinosaur/@slug', function ($slug) {
    $data = [
        'dino' => getDinoInfos($slug),
        'top_rated_dinos' => randomDinos(),
    ];
    Flight::view()->display('dino.twig', $data);
});

Flight::start();
