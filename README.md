# TP noté Jurassic Park

Site pour dinosaures (et non ordinosaures) que je n'ai pas connu malgré mon âge

## How to ?

1. `composer install`
2. `vendor/bin/phpunit tests/`
3. 127.0.0.1:8000
4. Et go sur [127.0.0.1:8000](http://127.0.0.1:8000)

## Tests

`vendor/bin/phpunit tests/`

## Requirements

```
mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown
```


## Authors

> [Sébastien CORSYN]
